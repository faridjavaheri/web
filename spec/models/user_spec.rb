# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  role                   :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Enumerations' do
    it { is_expected.to respond_to :admin? }
    it { is_expected.to respond_to :writer? }
    it { is_expected.to respond_to :guest? }
  end

  describe "Methods" do
    context ".full_name" do
      it "should return full_name" do
        user = create(:user, first_name: 'farid', last_name: 'javaheri')

        expect(user.full_name).to  eql('farid javaheri')
      end
    end
  end

  describe "Validations" do
    context "first_name" do
      it "should return error" do
        user = User.new
        user.first_name= ''
        expect(user.valid?).to eql(false)
        expect(user.errors.messages[:first_name]).to eql(["can't be blank"])
      end
    end
  end
end
